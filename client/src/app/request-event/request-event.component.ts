import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { DataService } from "../services/data.service";
import { Guru, EventRequest, TimeSlot, User } from "../types";
import { ToastrService } from "ngx-toastr";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

@Component({
	selector: "app-request-event",
	templateUrl: "./request-event.component.html",
	styleUrls: ["./request-event.component.scss"]
})
export class RequestEventComponent implements OnInit {
	currentUser!: User;
	guru!: Guru;
	courseId!: string;
	courseName!: string;
	suggestedSlots: TimeSlot[] = [];
	slotIndex = 0;
	userInput = "";
	link = "";
	tomorrow!: Date;

	socket!: Socket;

	constructor(
		private dataService: DataService,
		private socketService: SocketService,
		private toastr: ToastrService,
		public dialogRef: MatDialogRef<RequestEventComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { selectedGuru: Guru; courseId: string }
	) {
		this.guru = data.selectedGuru;
		this.courseId = data.courseId;
		const today = new Date();
		this.tomorrow = new Date(today);
		this.tomorrow.setDate(this.tomorrow.getDate() + 1);
		this.suggestedSlots.push({
			index: this.slotIndex,
			date: this.tomorrow,
			time: this.formatAMPM(this.tomorrow)
		});
	}

	async ngOnInit(): Promise<void> {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.currentUser = await this.dataService.getLoggedInUser();
		this.courseName = (
			await this.dataService.getCourse(this.courseId)
		).name;
	}

	cancel(): void {
		this.dialogRef.close();
	}

	async request(): Promise<void> {
		const times: Date[] = [];
		this.suggestedSlots.forEach((slot) => {
			times.push(new Date(`${slot.date.toDateString()} ${slot.time}`));
		});
		const request: EventRequest = {
			message: this.userInput,
			courseId: this.courseId,
			requesterId: this.currentUser._id,
			guruId: this.guru._id,
			suggestedTimes: times,
			link: this.link
		};
		await this.dataService.saveRequest(request);
		this.toastr.success("Your request has been sent", "", {
			positionClass: "toast-bottom-center"
		});
		this.socket.emit("send_guru_request", this.guru.user._id);
		this.cancel();
	}

	plusSlot(): void {
		this.slotIndex++;
		this.suggestedSlots.push({
			index: this.slotIndex,
			date: this.tomorrow,
			time: this.formatAMPM(this.tomorrow)
		});
	}

	minusSlot(index: number): void {
		this.suggestedSlots = this.suggestedSlots.filter(
			(slot) => slot.index !== index
		);
	}

	formatAMPM(date: Date): string {
		let hours = date.getHours();
		const minutes = date.getMinutes();
		const ampm = hours >= 12 ? "pm" : "am";
		hours = hours % 12;
		hours = hours ? hours : 12;
		const m = minutes < 10 ? "0" + minutes.toString() : minutes.toString();
		const strTime = hours.toString() + ":" + m + " " + ampm;
		return strTime;
	}

	isInputValid(): boolean {
		if (this.userInput === "" || this.link === "") {
			return false;
		}
		return true;
	}
}
