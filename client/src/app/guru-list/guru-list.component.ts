import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Observable } from "rxjs";
import { Guru } from "../types";

@Component({
	selector: "app-guru-list",
	templateUrl: "./guru-list.component.html",
	styleUrls: ["./guru-list.component.scss"]
})
export class GuruListComponent implements OnInit {
	@Input() gurus!: Observable<Guru[]>;
	@Output() guruRequest = new EventEmitter<Guru>();

	constructor() {}

	ngOnInit(): void {}

	request(guru: Guru): void {
		this.guruRequest.emit(guru);
	}
}
