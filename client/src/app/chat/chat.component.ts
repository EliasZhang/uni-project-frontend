import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	Input,
	OnChanges,
	OnInit,
	ViewChild
} from "@angular/core";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";
import { DataService } from "../services/data.service";
import { ScreenSizeService } from "../services/screen-size.service";
import { Message, MessageIsReadBy, User } from "../types";

@Component({
	selector: "app-chat",
	templateUrl: "./chat.component.html",
	styleUrls: ["./chat.component.scss"]
})
export class ChatComponent implements OnInit, OnChanges {
	@Input() courseId!: string;
	@Input() isPrivate!: boolean;
	@Input() receiverId!: string;
	@Input() groupChatMessages?: Message[];
	@Input() member_ids!: string[];

	socket!: Socket;
	userInput!: string;
	messages: Message[] = [];
	isTyping: User[] = [];

	@ViewChild("chatContainer")
	private chatContainer!: ElementRef<HTMLDivElement>;

	currentUser!: User;

	socketRouting!: string;
	socketEmitRouting!: string;

	constructor(
		private socketService: SocketService,
		public screenSizeService: ScreenSizeService,
		private changeDetector: ChangeDetectorRef,
		private dataService: DataService
	) {}

	async ngOnInit(): Promise<void> {
		this.currentUser = await this.dataService.getLoggedInUser();
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.socketRouting = "private-post";
		this.socketEmitRouting = "private-message";
		this.setupSocketConnection();
		this.socket.on("msg_read_live", (data: MessageIsReadBy) => {
			this.messages
				.find((m) => m._id === data.msgId)
				?.readBy?.push(data.readBy);
		});
		this.socket.on("user_is_typing", (user: User) => {
			if (user._id !== this.currentUser._id) {
				if (this.isTyping.every(({ _id }) => _id !== user._id)) {
					this.isTyping.push(user);
					setTimeout(() => this.removeTypingUser(user._id), 2000);
				}
			}
		});
		void (await this.getMessages());
		this.scrollToBottom();
	}

	async ngOnChanges(): Promise<void> {
		if (this.currentUser) {
			void (await this.getMessages());
		}
	}

	ngOnDestroy(): void {
		this.socket.off();
	}

	setupSocketConnection(): void {
		if (this.isPrivate) {
			this.socket.emit(
				"subscribe",
				this.generateRoomId(
					this.currentUser._id,
					this.receiverId,
					this.courseId
				)
			);
		} else {
			this.socket.emit("subscribe", this.courseId);
		}
		this.socket.on(this.socketRouting, async (data: Message) => {
			if (data) {
				data.time = new Date(data.time);
				this.messages.push(data);
				void (await this.markMsgAsRead());
				this.scrollToBottom();
			}
		});
	}

	generateRoomId(id1: string, id2: string, id3: string): string {
		return [id1, id2].sort().join("_") + id3;
	}

	sendMessage(): void {
		if (this.userInput.trim()) {
			const message = {
				senderId: this.currentUser._id,
				receiverId: this.isPrivate ? this.receiverId : undefined,
				courseId: this.courseId,
				message: this.userInput,
				time: new Date()
			};
			this.socket.emit(this.socketEmitRouting, message);
		}
		this.userInput = "";
		if (this.isPrivate) {
			this.socket.emit("send_private_msg", {
				from: this.currentUser._id,
				to: this.receiverId,
				courseId: this.courseId
			});
		} else {
			this.socket.emit("send_group_msg");
		}
	}

	async getMessages(): Promise<void> {
		if (this.isPrivate) {
			this.messages = await this.dataService.getMessagesPrivateChat(
				this.courseId,
				this.currentUser._id,
				this.receiverId
			);
		} else {
			this.messages = this.groupChatMessages || [];
		}
		if (this.messages.length > 0) void (await this.markMsgAsRead());
		this.scrollToBottom();
	}

	private scrollToBottom() {
		this.changeDetector.detectChanges();
		try {
			this.chatContainer.nativeElement.scrollTop =
				this.chatContainer.nativeElement.scrollHeight;
		} catch (err) {
			console.log(err);
		}
	}

	async markMsgAsRead(): Promise<void> {
		const readMsgIds: string[] = [];
		this.messages.forEach((msg) => {
			if (msg.user._id !== this.currentUser._id) {
				const user = msg.readBy?.filter((u) => {
					return u._id === this.currentUser._id;
				});
				if (user!.length === 0) {
					readMsgIds.push(msg._id!);
				}
			}
		});
		if (readMsgIds.length > 0) {
			await this.dataService.markMsgsAsRead(readMsgIds);
			readMsgIds.forEach((id) => {
				this.socket.emit("msg_read_live", {
					msgId: id,
					readBy: this.currentUser
				});
			});
		}
	}

	whoReadMessage(msg: Message): string {
		const userNames: string[] = [];
		msg.readBy?.forEach((user) => {
			userNames.push(user.name);
		});
		return userNames.join("\n");
	}

	isUserSender(message: Message): boolean {
		return this.currentUser?._id === message.user._id;
	}

	handleKeyUp(e: KeyboardEvent): void {
		if (e.key === "Enter") {
			this.sendMessage();
		} else {
			this.socket.emit("user_is_typing", this.currentUser);
		}
	}

	getUserTypingText(): string {
		return `${this.isTyping.map((user) => user.name).join(", ")} ${
			this.isTyping.length > 1 ? "are" : "is"
		} typing...`;
	}

	removeTypingUser(userId: string): void {
		this.isTyping = this.isTyping.filter(({ _id }) => _id !== userId);
	}
}
