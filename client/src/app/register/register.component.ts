import { HttpErrorResponse } from "@angular/common/http";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { DataService } from "../services/data.service";
import { UserData } from "../types";

@Component({
	selector: "app-register",
	templateUrl: "./register.component.html",
	styleUrls: ["./register.component.scss"]
})
export class RegisterComponent {
	constructor(
		private dataService: DataService,
		private router: Router,
		private toastr: ToastrService
	) {}

	async registerUser(userData: UserData): Promise<void> {
		try {
			await this.dataService.postRegisterUser(userData);
			void this.router.navigate(["/setup"], {
				queryParams: { initial: true }
			});
		} catch (e) {
			this.toastr.error((e as HttpErrorResponse).error, "", {
				positionClass: "toast-bottom-center"
			});
		}
	}
}
