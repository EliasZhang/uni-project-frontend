import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { RegisterComponent } from "./register/register.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./login/login.component";
import { CourseViewComponent } from "./course-view/course-view.component";
import { NavigationComponent } from "./navigation/navigation.component";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { GuruEventsComponent } from "./guru-events/guru-events.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { CompletedCoursesComponent } from "./completed-courses/completed-courses.component";
import { NotFoundComponent } from "./not-found/not-found.component";

const routes: Routes = [
	{ path: "login", component: LoginComponent },
	{ path: "register", component: RegisterComponent },
	{ path: "setup", component: CompletedCoursesComponent },
	{ path: "", component: LandingPageComponent },
	{
		path: "",
		component: NavigationComponent,
		children: [
			{ path: "", redirectTo: "dashboard", pathMatch: "full" },
			{ path: "dashboard", component: DashboardComponent },
			{ path: "course/:id", component: CourseViewComponent },
			{ path: "edit-profile", component: EditProfileComponent },
			{ path: "events", component: GuruEventsComponent },
			{ path: "completed-courses", component: CompletedCoursesComponent }
		]
	},
	{ path: "404", component: NotFoundComponent },
	{ path: "**", redirectTo: "/404" }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
