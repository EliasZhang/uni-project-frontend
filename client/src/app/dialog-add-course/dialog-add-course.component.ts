import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";
import { Course } from "../types";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

export interface DialogData {
	name: string;
}

@Component({
	selector: "app-dialog-add-course",
	templateUrl: "./dialog-add-course.component.html",
	styleUrls: ["./dialog-add-course.component.scss"]
})
export class DialogAddCourseComponent implements OnInit {
	joiningPossible = false;
	addingPossible = false;
	userInput = "";
	currentCourse?: Course;
	allCourses: Course[] = [];
	socket!: Socket;

	constructor(
		private dataService: DataService,
		private socketService: SocketService,
		public dialogRef: MatDialogRef<DialogAddCourseComponent>,
		public matDialog: MatDialog,
		private router: Router
	) {}

	async ngOnInit(): Promise<void> {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.allCourses = await this.dataService.getAllJoinableCourses();
	}

	async addCourse(): Promise<void> {
		const course = await this.dataService.postCreateCourse(this.userInput);
		void this.router.navigateByUrl(`/course/${course._id}`);
		this.cancel();
	}

	async joinCourse(): Promise<void> {
		if (this.currentCourse) {
			await this.dataService.postEnrollUser(this.currentCourse._id);
			this.socket.emit("somebody_joined_course");
			void this.router.navigateByUrl(`/course/${this.currentCourse._id}`);
			this.cancel();
		}
	}

	setPossibleActions(userInput: string | Course): void {
		if (typeof userInput === "string") {
			this.addingPossible = userInput.length > 2;
			this.joiningPossible = false;
			this.userInput = userInput;
		} else {
			this.addingPossible = false;
			this.joiningPossible = true;
			this.currentCourse = userInput;
		}
	}

	cancel(): void {
		this.dialogRef.close();
	}
}
