import { Component, Inject, OnInit, Optional } from "@angular/core";
import { FormControl } from "@angular/forms";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { Observable, of } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { RequestEventComponent } from "../request-event/request-event.component";
import { Guru } from "../types";

@Component({
	selector: "app-find-guru",
	templateUrl: "./find-guru.component.html",
	styleUrls: ["./find-guru.component.scss"]
})
export class FindGuruComponent implements OnInit {
	gurus!: Guru[];
	searchField = new FormControl();
	filteredGurus!: Observable<Guru[]>;
	courseId!: string;

	constructor(
		public dialogRef: MatDialogRef<FindGuruComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { guruList: Guru[]; courseId: string }
	) {
		this.courseId = data.courseId;
		this.gurus = data.guruList;
		this.filteredGurus = of(data.guruList);
	}

	ngOnInit(): void {
		this.filteredGurus = this.searchField.valueChanges.pipe(
			startWith(""),
			map((name: string) =>
				name ? this.filter(name) : this.gurus.slice()
			)
		);
	}

	private filter(name: string): Guru[] {
		const filterValue = name.toLowerCase();
		return this.gurus.filter((guru) =>
			guru.user.name.toLowerCase().includes(filterValue)
		);
	}

	cancel(): void {
		this.dialogRef.close();
	}

	request(guru: Guru): void {
		this.dialogRef.close();
		this.matDialog.open(RequestEventComponent, {
			data: { selectedGuru: guru, courseId: this.courseId }
		});
	}
}
