import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DataService } from "../services/data.service";
import { UserData } from "../types";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ToastrService } from "ngx-toastr";

@Component({
	selector: "app-edit-profile",
	templateUrl: "./edit-profile.component.html",
	styleUrls: ["./edit-profile.component.scss"]
})
export class EditProfileComponent implements OnInit {
	userData?: UserData;

	constructor(
		private dataService: DataService,
		private router: Router,
		private toastr: ToastrService
	) {}

	async ngOnInit(): Promise<void> {
		this.userData = await this.dataService.getProfileData();
	}

	async editUserData(userData: UserData): Promise<void> {
		await this.dataService.postEditProfile(userData);
		this.toastr.success("Your data have been changed", "", {
			positionClass: "toast-bottom-center"
		});
		this.userData = await this.dataService.getProfileData();
		//void this.router.navigate(["/edit-profile"]);
	}
}

export class SnackBarOverviewExample {
	constructor(private _snackBar: MatSnackBar) {}

	openSnackBar(message: string, action: string): void {
		this._snackBar.open(message, action);
	}
}
