import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output
} from "@angular/core";
import { Course } from "../types";

@Component({
	selector: "app-find-course",
	templateUrl: "./find-course.component.html",
	styleUrls: ["./find-course.component.scss"]
})
export class FindCourseComponent implements OnInit, OnChanges {
	@Input() courses: Course[] = [];
	@Input() initialValue?: Course;
	@Output() courseSelection = new EventEmitter<Course>();
	@Output() searchStringChange = new EventEmitter<string>();
	filteredCourses!: Course[];
	userInput: string | Course = "";

	constructor() {}

	ngOnInit(): void {
		this.filteredCourses = [...this.courses];
		if (this.initialValue) {
			this.userInput = this.initialValue;
		}
	}

	ngOnChanges(): void {
		if (this.initialValue) {
			this.userInput = this.initialValue;
		} else {
			this.userInput = "";
		}
		this.filteredCourses = [...this.courses];
	}

	filterCourses(searchString: string): void {
		this.filteredCourses = this.courses.filter((c) =>
			c.name.toLowerCase().includes(searchString.toLowerCase())
		);
	}

	displayFn(course: Course): string {
		return course?.name;
	}

	onInput(): void {
		if (typeof this.userInput === "string") {
			this.filterCourses(this.userInput);
			if (
				this.filteredCourses.length === 1 &&
				this.filteredCourses[0].name === this.userInput
			) {
				this.courseSelection.emit(this.filteredCourses[0]);
			} else {
				this.searchStringChange.emit(this.userInput);
			}
		} else {
			this.courseSelection.emit(this.userInput);
		}
	}
}
