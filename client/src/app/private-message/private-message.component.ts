import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { Message, User } from "../types";

@Component({
	selector: "app-private-message",
	templateUrl: "./private-message.component.html",
	styleUrls: ["./private-message.component.scss"]
})
export class PrivateMessageComponent implements OnInit {
	user!: User;
	userInput!: string;
	messages: Message[] = [];
	courseId!: string;

	constructor(
		public dialogRef: MatDialogRef<PrivateMessageComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { user: User; currentCourseId: string }
	) {
		this.user = data.user;
		this.courseId = data.currentCourseId;
	}

	ngOnInit(): void {}

	cancel(): void {
		this.dialogRef.close();
	}
}
