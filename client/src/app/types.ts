export interface User {
	_id: string;
	name: string;
	profileImageId?: string;
}

export interface Course {
	_id: string;
	name: string;
	guru_ids?: string[];
	member_ids: string[];
}

export interface Guru {
	_id: string;
	user: User;
	level: Proficiency;
	karma: number;
}

export interface UserData {
	_id?: string;
	name: string;
	password?: string;
	email: string;
	profileImg?: File;
	profileImageId?: string;
}

export interface TimeSlot {
	index: number;
	date: Date;
	time: string;
}

export interface Message {
	_id?: string;
	text: string;
	user: User;
	time: Date;
	readBy?: User[];
}

export interface MessageIsReadBy {
	msgId: string;
	readBy: User;
}

export interface MessageCount {
	count: number;
	senderIds: { senderId: string }[];
}

export interface RequestedEvent {
	_id: string;
	message: string;
	course: Course;
	requesterId: string;
	guru: Guru;
	suggestedTimes: Date[];
	link: string;
}

export interface EventRequest {
	message: string;
	courseId: string;
	requesterId: string;
	guruId: string;
	suggestedTimes: Date[];
	link: string;
}

export interface GuruEvent {
	_id?: string;
	message: string;
	courseId: string;
	courseName?: string;
	guruId: string;
	requesterId: string;
	guruName?: string;
	time: Date | undefined;
	link: string;
	isMine?: boolean;
}

export interface AcceptedEvent {
	_id?: string;
	message: string;
	course: Course;
	guru: Guru;
	requesterId: string;
	time: Date | undefined;
	link: string;
}

export enum Proficiency {
	"Basic",
	"Aced it",
	"Pretty proficient"
}

export interface Guruship {
	_id?: string;
	course: Course;
	proficiency: string;
}

export interface ImageHolder {
	data: string;
	contentType: string;
}

export interface MsgNotification {
	courseId: string;
	to: string;
	from: string;
}
