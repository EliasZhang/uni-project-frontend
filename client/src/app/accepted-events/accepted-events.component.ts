import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { DataService } from "../services/data.service";
import { AcceptedEvent, GuruEvent } from "../types";

@Component({
	selector: "app-accepted-events",
	templateUrl: "./accepted-events.component.html",
	styleUrls: ["./accepted-events.component.scss"]
})
export class AcceptedEventsComponent implements OnInit {
	acceptedEvents: GuruEvent[] = [];

	constructor(
		private dataService: DataService,
		private toastr: ToastrService,
		public dialogRef: MatDialogRef<AcceptedEventsComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { acceptedEvents: AcceptedEvent[] }
	) {
		this.acceptedEvents = data.acceptedEvents.map((e) => {
			return {
				_id: e._id,
				message: e.message,
				courseId: e.course._id,
				courseName: e.course.name,
				guruId: e.guru._id,
				requesterId: e.requesterId,
				guruName: e.guru.user.name,
				time: e.time,
				link: e.link,
				isMine: true
			};
		});
	}

	ngOnInit(): void {
		this.toastr.success("Selected events have been deleted", "", {
			positionClass: "toast-bottom-center"
		});
	}

	cancel(): void {
		this.dialogRef.close();
	}
}
