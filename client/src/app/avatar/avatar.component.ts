import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { ImageHolder, User } from "../types";
import { DataService } from "../services/data.service";

@Component({
	selector: "app-avatar",
	templateUrl: "./avatar.component.html",
	styleUrls: ["./avatar.component.scss"]
})
export class AvatarComponent implements OnInit, OnChanges {
	@Input() user?: User;
	@Input() isCurrentUser = false;
	image?: ImageHolder;
	imgURL?: string;

	constructor(private dataService: DataService) {}

	ngOnChanges(): void {
		if (this.user) {
			void this.getProfileImageURL();
		}
	}

	ngOnInit(): void {}

	async getProfileImageURL(): Promise<void> {
		if (this.user?.profileImageId) {
			const imageExists = await this.dataService.imageExists(
				this.user?.profileImageId
			);
			if (imageExists) {
				this.imgURL = `${this.dataService.getProfileImageBaseURL()}${
					this.user?.profileImageId
				}`;
			}
		}
	}
}
