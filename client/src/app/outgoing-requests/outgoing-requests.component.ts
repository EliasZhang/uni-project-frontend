import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { DataService } from "../services/data.service";
import { SocketService } from "../services/socket.service";
import { RequestedEvent } from "../types";
import { io, Socket } from "socket.io-client";

@Component({
	selector: "app-outgoing-requests",
	templateUrl: "./outgoing-requests.component.html",
	styleUrls: ["./outgoing-requests.component.scss"]
})
export class OutgoingRequestsComponent implements OnInit {
	requests!: RequestedEvent[];
	requestsToDelete: RequestedEvent[] = [];
	socket!: Socket;

	constructor(
		private dataService: DataService,
		private socketService: SocketService,
		private toastr: ToastrService,
		public dialogRef: MatDialogRef<OutgoingRequestsComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { requests: RequestedEvent[] }
	) {
		this.requests = data.requests;
	}

	ngOnInit(): void {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
	}

	cancel(): void {
		this.dialogRef.close();
	}

	async delete(): Promise<void> {
		const ids: string[] = [];
		this.requestsToDelete.forEach((request) => {
			ids.push(request._id);
		});
		await this.dataService.deleteEventRequest(ids);
		this.toastr.success("Selected requests have been deleted", "", {
			positionClass: "toast-bottom-center"
		});
		this.socket.emit("delete_outgoing_request");
		this.cancel();
	}

	selectRequest(request: RequestedEvent): void {
		if (!this.requestsToDelete.includes(request)) {
			this.requestsToDelete.push(request);
		} else {
			this.requestsToDelete.forEach((element, index) => {
				if (element == request) this.requestsToDelete.splice(index, 1);
			});
		}
	}

	isRequestSelected(request: RequestedEvent): boolean {
		if (this.requestsToDelete.includes(request)) {
			return true;
		}
		return false;
	}
}
