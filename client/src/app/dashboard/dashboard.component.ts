import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DataService } from "../services/data.service";
import { EventBusService } from "../services/event-bus.service";
import { EventService } from "../services/event.service";
import { Course, GuruEvent } from "../types";
import { SocketService } from "../services/socket.service";
import { io, Socket } from "socket.io-client";
@Component({
	selector: "app-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
	coursesList: Course[] = [];
	upcomingEvents: GuruEvent[] = [];
	pastEvents: GuruEvent[] = [];
	loading = true;
	socket!: Socket;

	constructor(
		private dataService: DataService,
		private eventBusService: EventBusService,
		public matDialog: MatDialog,
		private eventService: EventService,
		private socketService: SocketService
	) {}

	async ngOnInit(): Promise<void> {
		this.coursesList = await this.dataService.getCoursesForUser();
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		await this.showInfo();
		this.eventBusService.on("saveEvents", async () => {
			await this.showInfo();
		});
		this.eventBusService.on("deleteEvents", async () => {
			await this.showInfo();
		});
		this.eventBusService.on("deletePastEvent", async () => {
			await this.showInfo();
		});
		this.socket.on("events_update", async () => {
			await this.showInfo();
		});
	}

	async showInfo(): Promise<void> {
		const events = await this.dataService.getEventsForCurrentUser();
		const acceptedRequests = await this.dataService.getAcceptedRequests();
		this.upcomingEvents = [];
		this.pastEvents = [];
		if (acceptedRequests) {
			const myEvents: GuruEvent[] = acceptedRequests.map((my) => {
				return {
					_id: my._id,
					message: my.message,
					courseId: my.course._id,
					courseName: my.course.name,
					guruId: my.guru._id,
					requesterId: my.requesterId,
					guruName: my.guru.user.name,
					time: my.time,
					link: my.link,
					isMine: true
				};
			});
			events.push(...myEvents);
		}
		events.forEach((e) => {
			if (this.eventService.isUpcoming(e)) {
				this.upcomingEvents.push(e);
			} else {
				this.pastEvents.push(e);
			}
		});
		this.loading = false;
	}
}
