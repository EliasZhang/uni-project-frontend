import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { AvatarComponent } from "./avatar/avatar.component";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./auth-interceptor";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatCardModule } from "@angular/material/card";
import { LayoutModule } from "@angular/cdk/layout";
import { CourseViewComponent } from "./course-view/course-view.component";
import { ChatComponent } from "./chat/chat.component";
import { MemberListComponent } from "./member-list/member-list.component";
import { RequestEventComponent } from "./request-event/request-event.component";
import { MatDialogModule } from "@angular/material/dialog";
import { FindGuruComponent } from "./find-guru/find-guru.component";
import { MatNativeDateModule } from "@angular/material/core";
import { GuruListComponent } from "./guru-list/guru-list.component";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NgxMatNativeDateModule } from "@angular-material-components/datetime-picker";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgxMaterialTimepickerModule } from "ngx-material-timepicker";
import { NavigationComponent } from "./navigation/navigation.component";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import { MatSelectModule } from "@angular/material/select";
import { MatRadioModule } from "@angular/material/radio";
import { GuruEventsComponent } from "./guru-events/guru-events.component";
import { PrivateMessageComponent } from "./private-message/private-message.component";
import { UserDataComponent } from "./user-data/user-data.component";
import { DialogAddCourseComponent } from "./dialog-add-course/dialog-add-course.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatChipsModule } from "@angular/material/chips";
import { AnswerRequestComponent } from "./answer-request/answer-request.component";
import { LandingPageComponent } from "./landing-page/landing-page.component";
import { ToastrModule } from "ngx-toastr";
import { CompleteCourseDialogComponent } from "./complete-course-dialog/complete-course-dialog.component";
import { CompletedCoursesComponent } from "./completed-courses/completed-courses.component";
import { RateEventComponent } from "./rate-event/rate-event.component";
import { OutgoingRequestsComponent } from "./outgoing-requests/outgoing-requests.component";
import { FindCourseComponent } from "./find-course/find-course.component";
import { AcceptedEventsComponent } from "./accepted-events/accepted-events.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { CourseTileComponent } from "./course-tile/course-tile.component";
import { EventInfoComponent } from "./event-info/event-info.component";
import { MatTooltipModule } from "@angular/material/tooltip";

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		RegisterComponent,
		DashboardComponent,
		AvatarComponent,
		DashboardComponent,
		CourseViewComponent,
		ChatComponent,
		MemberListComponent,
		RequestEventComponent,
		FindGuruComponent,
		GuruListComponent,
		EditProfileComponent,
		GuruEventsComponent,
		PrivateMessageComponent,
		UserDataComponent,
		GuruEventsComponent,
		DialogAddCourseComponent,
		AnswerRequestComponent,
		LandingPageComponent,
		CourseTileComponent,
		NavigationComponent,
		CompleteCourseDialogComponent,
		CompletedCoursesComponent,
		RateEventComponent,
		OutgoingRequestsComponent,
		FindCourseComponent,
		AcceptedEventsComponent,
		NotFoundComponent,
		EventInfoComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatMenuModule,
		MatGridListModule,
		MatCardModule,
		LayoutModule,
		MatDialogModule,
		MatDatepickerModule,
		NgxMaterialTimepickerModule,
		MatNativeDateModule,
		NgxMatNativeDateModule,
		FormsModule,
		MatSidenavModule,
		MatListModule,
		MatSelectModule,
		MatRadioModule,
		HttpClientModule,
		MatRadioModule,
		MatAutocompleteModule,
		MatProgressSpinnerModule,
		ToastrModule.forRoot(),
		MatChipsModule,
		MatTooltipModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
