import { Component, OnInit } from "@angular/core";
import { Course, RequestedEvent, User } from "../types";
import { DataService } from "../services/data.service";
import { ScreenSizeService } from "../services/screen-size.service";
import { MatDialog } from "@angular/material/dialog";
import { AnswerRequestComponent } from "../answer-request/answer-request.component";
import { DialogAddCourseComponent } from "../dialog-add-course/dialog-add-course.component";
import { Router } from "@angular/router";
import { OutgoingRequestsComponent } from "../outgoing-requests/outgoing-requests.component";
import { EventBusService } from "../services/event-bus.service";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

@Component({
	selector: "app-navigation",
	templateUrl: "./navigation.component.html",
	styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements OnInit {
	currentUser?: User;
	courses: Course[] = [];
	requestedEvents: RequestedEvent[] = [];
	requests: RequestedEvent[] = [];
	socket!: Socket;

	constructor(
		private screenSizeService: ScreenSizeService,
		private dataService: DataService,
		private socketService: SocketService,
		private eventBusService: EventBusService,
		public matDialog: MatDialog,
		private router: Router
	) {}

	async ngOnInit(): Promise<void> {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.currentUser = await this.dataService.getLoggedInUser();
		this.courses = await this.dataService.getCoursesForUser();
		this.requestedEvents = await this.dataService.getEventsByUserId(
			this.currentUser._id
		);
		this.requests = await this.dataService.getOutgoingRequests(
			this.currentUser._id
		);
		this.eventBusService.on("postEditProfile", async () => {
			const user = await this.dataService.getProfileData();
			this.currentUser = {
				_id: user._id || "",
				name: user.name,
				profileImageId: user.profileImageId
			};
		});
		this.eventBusService.on("saveRequest", async () => {
			this.requests = await this.dataService.getOutgoingRequests(
				this.currentUser!._id
			);
		});
		this.eventBusService.on("deleteEventRequest", async () => {
			this.requests = await this.dataService.getOutgoingRequests(
				this.currentUser!._id
			);
		});
		this.socket.on(
			"guru_request_created_" + this.currentUser._id,
			async () => {
				this.requestedEvents = await this.dataService.getEventsByUserId(
					this.currentUser!._id
				);
			}
		);
		this.socket.on("delete_outgoing_request", async () => {
			this.requestedEvents = await this.dataService.getEventsByUserId(
				this.currentUser!._id
			);
		});
		this.socket.on("events_update", async () => {
			this.requests = await this.dataService.getOutgoingRequests(
				this.currentUser!._id
			);
		});
	}

	isMobile(): boolean {
		return this.screenSizeService.isMobile();
	}

	signOut(): void {
		localStorage.removeItem("jwt");
		this.dataService.clearCache();
		void this.router.navigate(["/login"]);
	}

	openNotifications(): void {
		const dialogRef = this.matDialog.open(AnswerRequestComponent, {
			data: { events: this.requestedEvents }
		});

		dialogRef.afterClosed().subscribe(async () => {
			this.requestedEvents = await this.dataService.getEventsByUserId(
				this.currentUser!._id
			);
		});
	}

	openOutcomingRequests(): void {
		this.matDialog.open(OutgoingRequestsComponent, {
			data: { requests: this.requests }
		});
	}

	openDialog(): void {
		const dialogRef = this.matDialog.open(DialogAddCourseComponent);
		dialogRef.afterClosed().subscribe(async () => {
			this.courses = await this.dataService.getCoursesForUser();
		});
	}

	async reloadCourses(): Promise<void> {
		this.courses = await this.dataService.getCoursesForUser();
	}
}
