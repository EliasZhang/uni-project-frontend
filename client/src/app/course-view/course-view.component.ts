import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Params } from "@angular/router";
import { CompleteCourseDialogComponent } from "../complete-course-dialog/complete-course-dialog.component";
import { FindGuruComponent } from "../find-guru/find-guru.component";
import { DataService } from "../services/data.service";
import { ScreenSizeService } from "../services/screen-size.service";
import { Course, Guru, Message, User } from "../types";
import { Router } from "@angular/router";
import { NavigationComponent } from "../navigation/navigation.component";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

@Component({
	selector: "app-course-view",
	templateUrl: "./course-view.component.html",
	styleUrls: ["./course-view.component.scss"]
})
export class CourseViewComponent implements OnInit {
	course!: Course;
	gurus!: Guru[];
	messages?: Message[];
	members?: User[];
	loading = true;
	member_ids: string[] = [];
	socket!: Socket;

	constructor(
		public dialog: MatDialog,
		private dataService: DataService,
		private socketService: SocketService,
		private route: ActivatedRoute,
		public matDialog: MatDialog,
		public screenSizeService: ScreenSizeService,
		private router: Router,
		private navigation: NavigationComponent
	) {}

	ngOnInit(): void {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.route.params.subscribe(async (routeParams: Params) => {
			this.loading = true;
			const courseId = routeParams["id"] as string;
			if (courseId) {
				this.course = await this.dataService.getCourse(courseId);
				this.gurus = await this.dataService.getGurusbyCourseId(
					courseId
				);
				this.messages = await this.dataService.getMessagesGroupChat(
					courseId
				);
				this.members = await this.dataService.getCourseMembers(
					courseId
				);
				this.members?.map((m) => {
					this.member_ids.push(m._id);
				});
				this.socket.on("somebody_joined_course", async () => {
					this.members = await this.dataService.getCourseMembers(
						courseId
					);
				});
				this.socket.on("leave_course", async () => {
					this.members = await this.dataService.getCourseMembers(
						courseId
					);
				});
				this.loading = false;
			} else {
				void this.router.navigateByUrl("/404");
			}
		});
	}

	getHelp(): void {
		this.matDialog.open(FindGuruComponent, {
			data: { guruList: this.gurus, courseId: this.course._id }
		});
	}

	async leaveCourse(): Promise<void> {
		await this.dataService.leaveCourse(this.course._id);
		void this.navigation.reloadCourses();
		void this.router.navigate(["/dashboard"]);
	}

	completeCourseAction(): void {
		const dialogRef = this.dialog.open(CompleteCourseDialogComponent);

		dialogRef.afterClosed().subscribe(async (result) => {
			if (result) {
				await this.dataService.leaveCourse(this.course._id);
				void this.navigation.reloadCourses();
				void this.router.navigate(["/completed-courses"], {
					queryParams: { courseId: this.course._id }
				});
			}
		});
	}
}
