import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { DataService } from "../services/data.service";
import { Course, Guruship, Proficiency } from "../types";

@Component({
	selector: "app-completed-courses",
	templateUrl: "./completed-courses.component.html",
	styleUrls: ["./completed-courses.component.scss"]
})
export class CompletedCoursesComponent implements OnInit {
	loading = true;
	guruships: Guruship[] = [];
	proficiencyLevels = Object.keys(Proficiency).filter((l) =>
		isNaN(Number(l))
	);
	proficiency?: string;
	course?: Course;
	updatedGuruships: Guruship[] = [];
	allCourses: Course[] = [];
	gurushipsChanged = false;
	courses: Course[] = [];
	isRegistrationStep = false;

	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		private router: Router,
		private toastr: ToastrService
	) {}

	async ngOnInit(): Promise<void> {
		this.guruships = (await this.dataService.getGuruships()) || [];
		this.updatedGuruships = [...this.guruships];
		this.allCourses = await this.dataService.getAllNotEnrolledCourses();
		this.courses = [...this.allCourses];
		const courseId = this.route.snapshot.queryParamMap.get("courseId");
		if (courseId) {
			this.course = this.courses.find((c) => c._id === courseId);
		}
		if (this.route.snapshot.queryParamMap.get("initial")) {
			this.isRegistrationStep = true;
		}
		this.updateCourses();
		this.loading = false;
	}

	setCourse(selectedCourse: Course): void {
		this.course = selectedCourse;
	}

	addGuruship(): void {
		if (this.course && this.proficiency) {
			this.updatedGuruships.push({
				course: this.course,
				proficiency: this.proficiency
			});
			this.course = undefined;
			this.proficiency = undefined;
			this.gurushipsChanged = true;
			this.updateCourses();
		}
	}

	private updateCourses(): void {
		this.courses = this.allCourses.filter(
			(course) =>
				!this.updatedGuruships.some(
					(guruship) => guruship.course._id === course._id
				)
		);
	}

	removeGuruship(index: number): void {
		this.updatedGuruships.splice(index, 1);
		this.gurushipsChanged = true;
		this.updateCourses();
	}

	async save(): Promise<void> {
		const guruships = [...this.updatedGuruships];
		if (this.course && this.proficiency) {
			guruships.push({
				course: this.course,
				proficiency: this.proficiency
			});
		}
		await this.dataService.postUpdateGuruships(guruships);
		this.addGuruship();
		await this.dataService.postUpdateGuruships(this.updatedGuruships);
		this.toastr.success("Completed courses have been updated", "", {
			positionClass: "toast-bottom-center"
		});
		this.gurushipsChanged = false;
		if (this.isRegistrationStep) {
			void this.router.navigate(["/dashboard"]);
		}
	}
}
