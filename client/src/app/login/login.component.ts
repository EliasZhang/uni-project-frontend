import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { DataService } from "../services/data.service";
@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
	loginFailed = false;

	loginForm = new FormGroup({
		email: new FormControl("", Validators.email),
		password: new FormControl("", Validators.minLength(8))
	});

	constructor(private dataService: DataService, private router: Router) {}

	ngOnInit(): void {}

	async onSubmit(): Promise<void> {
		const email = this.loginForm.get("email")?.value;
		const password = this.loginForm.get("password")?.value;
		if (email && password) {
			try {
				await this.dataService.postLogin(email, password);
				void this.router.navigate(["/dashboard"]);
			} catch (e) {
				this.loginFailed = true;
			}
		}
	}
}
