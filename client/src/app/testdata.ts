import { UserData, Course, User } from "./types";

export const testCourses: Course[] = [
	{
		_id: "1",
		name: "Oxygen",
		guru_ids: [],
		member_ids: []
	}
];

export const testUsers: User[] = [
	{
		_id: "1",

		name: "Dugall"
	},
	{
		_id: "2",

		name: "Scranedge"
	}
];

export const testAccountData: UserData = {
	name: "Herrera Abril",
	email: "arcadio@gmail.com"
};
