import { Injectable } from "@angular/core";
import {
	HttpErrorResponse,
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest
} from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	public constructor(private router: Router, private toastr: ToastrService) {}

	intercept(
		request: HttpRequest<unknown>,
		next: HttpHandler
	): Observable<HttpEvent<unknown>> {
		return next.handle(request).pipe(
			catchError((response: HttpErrorResponse) => {
				if (response.status === 401) {
					void this.router.navigate(["/login"]);
				}
				if (response.status === 0) {
					this.toastr.error("The backend is not running.", "", {
						positionClass: "toast-bottom-center"
					});
				}
				return throwError(response);
			})
		);
	}
}
