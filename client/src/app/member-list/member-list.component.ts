import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { PrivateMessageComponent } from "../private-message/private-message.component";
import { DataService } from "../services/data.service";
import { MessageCount, MsgNotification, User } from "../types";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

@Component({
	selector: "app-member-list",
	templateUrl: "./member-list.component.html",
	styleUrls: ["./member-list.component.scss"]
})
export class MemberListComponent implements OnInit, OnChanges {
	@Input() courseId!: string;
	@Input() members?: User[];
	newMsgList: MessageCount = { senderIds: [], count: 0 };
	currentUser?: User;
	socket!: Socket;
	hasIncomingMsgFrom = "";

	constructor(
		private dataService: DataService,
		public matDialog: MatDialog,
		private socketService: SocketService
	) {}

	async ngOnInit(): Promise<void> {
		this.currentUser = await this.dataService.getLoggedInUser();
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.socket.on(
			"private_msg_notification_" + this.currentUser._id,
			(obj: MsgNotification) => {
				this.hasIncomingMsgFrom =
					obj.courseId === this.courseId ? obj.from : "";
			}
		);
	}

	async ngOnChanges(): Promise<void> {
		this.newMsgList = await this.dataService.getNewPrivateMessages(
			this.courseId
		);
	}

	newMsg(member: User): boolean {
		let result = false;
		if (this.newMsgList.count > 0) {
			this.newMsgList.senderIds.forEach((element) => {
				if (element.senderId.includes(member._id)) {
					result = true;
				}
			});
		}
		return result || this.hasIncomingMsgFrom === member._id;
	}

	async writeMessage(user: User): Promise<void> {
		if (this.hasIncomingMsgFrom === user._id) {
			this.newMsgList = await this.dataService.getNewPrivateMessages(
				this.courseId
			);
			this.hasIncomingMsgFrom = "";
		}
		const dialogRef = this.matDialog.open(PrivateMessageComponent, {
			data: { user: user, currentCourseId: this.courseId }
		});
		dialogRef.afterClosed().subscribe(async () => {
			this.newMsgList = await this.dataService.getNewPrivateMessages(
				this.courseId
			);
			if (this.hasIncomingMsgFrom === user._id) {
				this.newMsgList = await this.dataService.getNewPrivateMessages(
					this.courseId
				);
				this.hasIncomingMsgFrom = "";
			}
		});
	}
}
