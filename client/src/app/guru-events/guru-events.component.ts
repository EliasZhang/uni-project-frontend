import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { RateEventComponent } from "../rate-event/rate-event.component";
import { DataService } from "../services/data.service";
import { EventService } from "../services/event.service";
import { GuruEvent, User } from "../types";

@Component({
	selector: "app-guru-events",
	templateUrl: "./guru-events.component.html",
	styleUrls: ["./guru-events.component.scss"]
})
export class GuruEventsComponent implements OnInit {
	@Input() event!: GuruEvent;
	requester!: User;

	constructor(
		public eventService: EventService,
		public matDialog: MatDialog,
		private dataService: DataService,
		private toastr: ToastrService
	) {}

	async ngOnInit(): Promise<void> {
		this.requester = await this.dataService.getUserById(
			this.event.requesterId
		);
	}

	rateGuru(event: GuruEvent): void {
		this.matDialog.open(RateEventComponent, {
			data: { event: event }
		});
	}

	async deleteEvent(event: GuruEvent): Promise<void> {
		const ids: (string | undefined)[] = [event._id];
		await this.dataService.deleteEvents(ids);
		this.toastr.success("Selected event has been deleted", "", {
			positionClass: "toast-bottom-center"
		});
	}

	async deletePastEvent(): Promise<void> {
		await this.dataService.deletePastEvent(this.event._id);
	}
}
