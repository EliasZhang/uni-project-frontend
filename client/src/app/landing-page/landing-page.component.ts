import { Component, OnInit } from "@angular/core";
import { DataService } from "../services/data.service";

@Component({
	selector: "app-landing-page",
	templateUrl: "./landing-page.component.html",
	styleUrls: ["./landing-page.component.scss"]
})
export class LandingPageComponent implements OnInit {
	courseNames: string[] = [
		"Advanced Web Technologies",
		"Recommender Systems"
	];

	constructor(private dataService: DataService) {}

	async ngOnInit(): Promise<void> {
		const allCourseNames = await this.dataService.getAllCourseNames();
		if (allCourseNames.length > 1) {
			this.courseNames = allCourseNames.slice(allCourseNames.length - 2);
		}
	}
}
