import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { DataService } from "../services/data.service";
import { GuruEvent, Guru } from "../types";

@Component({
	selector: "app-rate-event",
	templateUrl: "./rate-event.component.html",
	styleUrls: ["./rate-event.component.scss"]
})
export class RateEventComponent implements OnInit {
	event!: GuruEvent;
	guru!: Guru;
	rated = false;

	constructor(
		private dataService: DataService,
		private toastr: ToastrService,
		public dialogRef: MatDialogRef<RateEventComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { event: GuruEvent }
	) {
		this.event = data.event;
	}

	async ngOnInit(): Promise<void> {
		this.guru = await this.dataService.getGuruByGuruId(this.event.guruId);
	}

	cancel(): void {
		this.dialogRef.close();
	}

	addKarma(): void {
		this.guru.karma++;
		this.rated = true;
	}

	pullOffKarma(): void {
		this.guru.karma--;
		this.rated = false;
	}

	async save(): Promise<void> {
		if (this.rated) {
			await this.dataService.updateGurusKarma(this.guru._id);
		}
		await this.dataService.deletePastEvent(this.event._id);
		this.toastr.success("The past event was deleted", "", {
			positionClass: "toast-bottom-center"
		});
		if (this.rated) {
			this.toastr.success("Thanks for your feedback!", "", {
				positionClass: "toast-bottom-center"
			});
		}
		this.cancel();
	}

	getButtonText(): string {
		return !this.rated ? "Delete past event" : "Rate and delete past event";
	}
}
