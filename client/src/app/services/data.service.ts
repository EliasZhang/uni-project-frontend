import { Injectable } from "@angular/core";
import { testCourses } from "../testdata";
import {
	UserData,
	Course,
	User,
	GuruEvent,
	Guru,
	RequestedEvent,
	EventRequest,
	Message,
	ImageHolder,
	Guruship,
	MessageCount,
	AcceptedEvent
} from "../types";
import {
	HttpClient,
	HttpErrorResponse,
	HttpParams
} from "@angular/common/http";
import { environment } from "../../environments/environment";
import { EventBusService } from "./event-bus.service";
import { io, Socket } from "socket.io-client";
import { SocketService } from "./socket.service";

@Injectable({
	providedIn: "root"
})
export class DataService {
	private loggedInUser?: User;
	socket!: Socket;
	constructor(
		private http: HttpClient,
		private eventBusService: EventBusService,
		private socketService: SocketService
	) {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
	}

	private async get<T>(path: string, httpParams?: HttpParams): Promise<T> {
		return this.http
			.get<T>(environment.BACKEND_URL + path, {
				params: this.paramsWithToken(httpParams)
			})
			.toPromise();
	}

	private async post<T>(path: string, body: unknown): Promise<T> {
		return this.http
			.post<T>(environment.BACKEND_URL + path, body, {
				params: this.paramsWithToken()
			})
			.toPromise();
	}

	private paramsWithToken(httpParams?: HttpParams) {
		let params = httpParams || new HttpParams();
		const token = localStorage.getItem("jwt");
		const privateKey = localStorage.getItem("key");
		if (token) {
			params = params.append("token", token);
		}
		if (privateKey) {
			params = params.append("privateKey", privateKey);
		}
		return params;
	}

	clearCache(): void {
		this.loggedInUser = undefined;
	}

	async getAllCourseNames(): Promise<string[]> {
		try {
			return this.get<string[]>("/courses/allCourseNames");
		} catch (e) {
			return Promise.resolve([]);
		}
	}

	async postLogin(email: string, password: string): Promise<void> {
		const tokenAndKey: { token: string; privateKey: string } =
			await this.post<{ token: string; privateKey: string }>(
				"/users/login",
				{
					email: email,
					password: password
				}
			);
		localStorage.setItem("jwt", tokenAndKey.token);
		localStorage.setItem("key", tokenAndKey.privateKey);
	}

	async postRegisterUser(userData: UserData): Promise<void> {
		const tokenAndKey: { token: string; privateKey: string } =
			await this.post<{ token: string; privateKey: string }>(
				"/users/register",
				{
					name: userData.name,
					email: userData.email,
					password: userData.password
				}
			);
		localStorage.setItem("jwt", tokenAndKey.token);
		localStorage.setItem("key", tokenAndKey.privateKey);
	}

	async getUserById(id: string): Promise<User> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get("/users/getUserById", params);
	}

	async postUploadImage(imageFile: File): Promise<ImageHolder> {
		const formData = new FormData();
		formData.append("profileImg", imageFile);
		return this.post<ImageHolder>("/users/uploadProfileImage", formData);
	}

	async postDeleteImage(): Promise<void> {
		return this.post<void>("/users/delete-profile-image", {});
	}

	getProfileImageBaseURL(): string {
		return environment.BACKEND_URL + "/";
	}

	async imageExists(imageId: string): Promise<boolean> {
		return this.get<void>(`/${imageId}`)
			.then((e) => true)
			.catch((e: HttpErrorResponse) => {
				if (e.status === 200) {
					return true;
				}
				return false;
			});
	}

	async postEditProfile(userData: UserData): Promise<void> {
		const formData = new FormData();
		if (userData.profileImg) {
			formData.append("profileImg", userData.profileImg as Blob);
		}
		if (userData.name) {
			formData.append("name", userData.name);
		}
		if (userData.email) {
			formData.append("email", userData.email);
		}
		if (userData.password) {
			formData.append("password", userData.password);
		}
		await this.post("/users/edit-profile", formData);
		this.eventBusService.emit("postEditProfile");
	}

	async postCreateCourse(courseName: string): Promise<Course> {
		return this.post<Course>("/courses/create", {
			name: courseName
		});
	}

	async postEnrollUser(courseId: string): Promise<User> {
		return this.post<User>("/courses/enrollUser", { courseId: courseId });
	}

	async postUpdateGuruships(guruships: Guruship[]): Promise<void> {
		return this.post("/gurus/updateGuruships", guruships);
	}

	async postDeleteGuruships(guruships: Guruship[]): Promise<void> {
		return this.post("/gurus/deleteGuruships", guruships);
	}

	async getGuruships(): Promise<Guruship[]> {
		return this.get("/gurus/gurushipsForCurrentUser");
	}

	async getCoursesForUser(): Promise<Course[]> {
		return this.get<Course[]>("/courses/forCurrentUser");
	}

	async getTestData(): Promise<User[]> {
		console.info(location.origin);
		const myValue = this.get<User[]>("/users");
		console.info(myValue);
		return myValue;
	}

	async getLoggedInUser(): Promise<User> {
		if (!this.loggedInUser) {
			this.loggedInUser = await this.get<User>("/users/loggedInUser");
		}
		return this.loggedInUser;
	}

	async getProfileData(): Promise<UserData> {
		return this.get<UserData>("/users/profileData");
	}

	async getCompletedCourses(): Promise<Course[]> {
		return Promise.resolve(testCourses.splice(0, 5));
	}

	async getAllNotEnrolledCourses(): Promise<Course[]> {
		return this.get<Course[]>("/courses/allNotEnrolled");
	}

	async getAllJoinableCourses(): Promise<Course[]> {
		return this.get<Course[]>("/courses/allNotEnrolledNotGuru");
	}

	async getCourseMembers(courseId: string): Promise<User[]> {
		let params = new HttpParams();
		params = params.append("courseId", courseId);
		return this.get<User[]>("/users/byCourseId", params);
	}

	async getCourse(id: string): Promise<Course> {
		let params = new HttpParams();
		params = params.append("courseId", id);
		return this.get<Course>("/courses/courseById", params);
	}

	async leaveCourse(id: string): Promise<void> {
		this.socket.emit("leave_course");
		return this.post("/courses/leaveCourse", { courseId: id });
	}

	async getGurus(ids: string[]): Promise<Guru[]> {
		let params = new HttpParams();
		params = params.append("ids", ids.join(", "));
		return this.get<Guru[]>("/gurus/gurusByIds", params);
	}

	async getGurusbyCourseId(id: string): Promise<Guru[]> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<Guru[]>("/gurus/gurusByCourseId", params);
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async getEventsByCourseId(id: string): Promise<GuruEvent[]> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<GuruEvent[]>("/events/getEventsByCourseId", params);
	}

	async getMessagesGroupChat(courseId: string): Promise<Message[]> {
		let params = new HttpParams();
		params = params.append("id", courseId);
		return this.get<Message[]>("/messages/groupChat", params);
	}

	async getMessagesPrivateChat(
		courseId: string,
		senderId: string,
		receiverId: string
	): Promise<Message[]> {
		let params = new HttpParams();
		params = params.appendAll({
			id: courseId,
			s_id: senderId,
			r_id: receiverId
		});
		return this.get<Message[]>("/messages/privateChat", params);
	}

	async saveRequest(request: EventRequest): Promise<void> {
		await this.post("/requestEvents/requestEvent", {
			message: request.message,
			courseId: request.courseId,
			requesterId: request.requesterId,
			guruId: request.guruId,
			suggestedTimes: request.suggestedTimes,
			link: request.link
		});
		this.eventBusService.emit("saveRequest");
	}

	async deleteRequest(requestId: string | undefined): Promise<void> {
		return this.post("/requestEvents/deleteRequestEvent", {
			requestId: requestId
		});
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	async getEventsByUserId(id: string): Promise<RequestedEvent[]> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<RequestedEvent[]>(
			"/requestEvents/getRequestsForUser",
			params
		);
	}

	async getEventsForCurrentUser(): Promise<GuruEvent[]> {
		return this.get<GuruEvent[]>("/events/getEventsForCurrentUser");
	}

	async getOutgoingRequests(id: string): Promise<RequestedEvent[]> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<RequestedEvent[]>(
			"/requestEvents/getOutgoingRequests",
			params
		);
	}

	async getAcceptedRequests(): Promise<AcceptedEvent[]> {
		return this.get<AcceptedEvent[]>("/events/getAcceptedRequestsForGuru");
	}

	async saveEvents(events: GuruEvent[]): Promise<void> {
		await this.post("/events/saveEvents", events);
		this.socket.emit("events_update");
		this.eventBusService.emit("saveEvents");
	}

	async getGuruByGuruId(id: string): Promise<Guru> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<Guru>("/gurus/getGuruByGuruId", params);
	}

	async deletePastEvent(id: string | undefined): Promise<void> {
		await this.post("/events/deleteEvent", {
			id: id
		});
		this.socket.emit("events_update");
		this.eventBusService.emit("deletePastEvent");
	}

	async deleteEvents(id: (string | undefined)[]): Promise<void> {
		await this.post("/events/deleteEvents", {
			id: id
		});
		this.socket.emit("events_update");
		this.eventBusService.emit("deleteEvents");
	}

	async deleteEventRequest(id: (string | undefined)[]): Promise<void> {
		await this.post("/requestEvents/deleteRequests", {
			id: id
		});
		this.eventBusService.emit("deleteEventRequest");
	}

	async updateGurusKarma(id: string): Promise<void> {
		return this.post("/gurus/updateGurusKarma", {
			id: id
		});
	}

	async markMsgsAsRead(ids: string[]): Promise<void> {
		return this.post("/messages/markMsgsAsRead", {
			ids: ids
		});
	}

	async getNewPrivateMessages(id: string): Promise<MessageCount> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<MessageCount>(
			"/messages/getNewPrivateMessages",
			params
		);
	}

	async getNewGroupMessagesCount(id: string): Promise<MessageCount> {
		let params = new HttpParams();
		params = params.append("id", id);
		return this.get<MessageCount>(
			"/messages/getNewGroupMessagesCount",
			params
		);
	}
}
