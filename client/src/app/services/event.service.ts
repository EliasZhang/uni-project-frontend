import { Injectable } from "@angular/core";
import { GuruEvent } from "../types";

@Injectable({
	providedIn: "root"
})
export class EventService {
	constructor() {}

	isUpcoming(event: GuruEvent): boolean {
		const now = new Date();
		if (new Date(event.time!) >= now) {
			return true;
		} else {
			return false;
		}
	}
}
