import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
	providedIn: "root"
})
export class SocketService {
	SOCKET_ENDPOINT = environment.BACKEND_URL;

	constructor() {}
}
