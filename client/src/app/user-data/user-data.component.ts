import { EventEmitter, OnInit } from "@angular/core";
import { Component, Input, Output } from "@angular/core";
import {
	AbstractControl,
	FormControl,
	FormGroup,
	ValidationErrors,
	ValidatorFn,
	Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { DataService } from "../services/data.service";
import { ImageHolder, UserData } from "../types";

@Component({
	selector: "app-user-data",
	templateUrl: "./user-data.component.html",
	styleUrls: ["./user-data.component.scss"]
})
export class UserDataComponent implements OnInit {
	@Input() isRegistration = false;
	@Input() userData?: UserData;
	@Output() submitUserData: EventEmitter<UserData> =
		new EventEmitter<UserData>();

	userDataForm!: FormGroup;
	matcher = new MyErrorStateMatcher();
	hide = true;
	profilePicture?: ImageHolder;
	pictureUpload?: File;
	previewImage?: string;

	constructor(private dataService: DataService) {}

	ngOnInit(): void {
		if (this.isRegistration) {
			this.userDataForm = new FormGroup(
				{
					name: new FormControl(null, Validators.required),
					email: new FormControl(null, [
						Validators.required,
						Validators.email
					]),
					password: new FormControl("", [
						Validators.required,
						Validators.minLength(8)
					]),
					confirmPassword: new FormControl("")
				},
				this.checkPasswords()
			);
		} else {
			this.userDataForm = new FormGroup(
				{
					name: new FormControl(null),
					email: new FormControl(null, [Validators.email]),
					password: new FormControl("", [Validators.minLength(8)]),
					confirmPassword: new FormControl("")
				},
				this.checkPasswords()
			);
		}
	}

	onSubmit(): void {
		const userData: UserData = {
			name: this.userDataForm.get("name")?.value,
			email: this.userDataForm.get("email")?.value,
			password: this.userDataForm.get("password")?.value,
			profileImg: this.pictureUpload
		};
		this.submitUserData.emit(userData);
		this.pictureUpload = undefined;
		this.userDataForm.markAsPristine();
	}

	upload(target: EventTarget | null): void {
		const element: HTMLInputElement = target as HTMLInputElement;
		if (element.files && element.files.length) {
			this.pictureUpload = element.files[0];
			const reader = new FileReader();
			reader.onload = (e) => {
				this.previewImage = e.target?.result as string;
			};
			reader.readAsDataURL(this.pictureUpload);
		}
	}

	clearImage(): void {
		this.pictureUpload = undefined;
		this.previewImage = undefined;
	}

	deleteImage(): void {
		void this.dataService.postDeleteImage();
		if (this.userData) {
			this.userData.profileImageId = undefined;
		}
	}

	public checkPasswords(): ValidatorFn {
		return (control: AbstractControl): ValidationErrors | null => {
			const password = control.get("password")?.value;
			const confirmPassword = control.get("confirmPassword")?.value;
			return password === confirmPassword ? null : { notSame: true };
		};
	}

	public getProfilePictureUrl(): string | undefined {
		const imageId = this.userData?.profileImageId;
		return imageId
			? `${this.dataService.getProfileImageBaseURL()}${imageId}`
			: undefined;
	}
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null): boolean {
		const invalidCtrl = !!(control?.invalid && control?.parent?.dirty);
		const invalidParent = !!(
			control?.parent?.invalid && control?.parent?.dirty
		);

		return invalidCtrl || invalidParent;
	}
}
