import { DataService } from "../services/data.service";
import { Component, Input, OnInit } from "@angular/core";
import { Course, MessageCount, User } from "../types";
import { EventService } from "../services/event.service";
import { io, Socket } from "socket.io-client";
import { SocketService } from "../services/socket.service";

@Component({
	selector: "app-course-tile",
	templateUrl: "./course-tile.component.html",
	styleUrls: ["./course-tile.component.scss"]
})
export class CourseTileComponent implements OnInit {
	@Input() course!: Course;
	futureEventsCount = 0;
	pastEventsCount = 0;
	newGroupMessages: MessageCount = { senderIds: [], count: 0 };
	newPrivateMessages: MessageCount = { senderIds: [], count: 0 };
	socket!: Socket;
	currentUser!: User;

	constructor(
		private dataService: DataService,
		private eventService: EventService,
		private socketService: SocketService
	) {}

	async ngOnInit(): Promise<void> {
		this.currentUser = await this.dataService.getLoggedInUser();
		void this.listenToEvets();
		void this.getEvents();
		this.newGroupMessages = await this.dataService.getNewGroupMessagesCount(
			this.course._id
		);
		this.newPrivateMessages = await this.dataService.getNewPrivateMessages(
			this.course._id
		);
	}

	async getEvents(): Promise<void> {
		const events = await this.dataService.getEventsByCourseId(
			this.course._id
		);
		events.forEach((event) => {
			if (this.eventService.isUpcoming(event)) {
				this.futureEventsCount++;
			} else {
				this.pastEventsCount++;
			}
		});
	}

	listenToEvets(): void {
		this.socket = io(this.socketService.SOCKET_ENDPOINT);
		this.socket.on(
			"private_msg_notification_" + this.currentUser._id,
			async () => {
				this.newPrivateMessages =
					await this.dataService.getNewPrivateMessages(
						this.course._id
					);
			}
		);
		this.socket.on("group_msg_notification", async () => {
			this.newGroupMessages =
				await this.dataService.getNewGroupMessagesCount(
					this.course._id
				);
		});
		this.socket.on("events_update", () => {
			this.futureEventsCount = 0;
			this.pastEventsCount = 0;
			void this.getEvents();
		});
	}
}
