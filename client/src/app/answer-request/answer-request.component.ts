import { Component, Inject, OnInit, Optional } from "@angular/core";
import {
	MatDialog,
	MatDialogRef,
	MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { DataService } from "../services/data.service";
import { GuruEvent, RequestedEvent } from "../types";
import { ToastrService } from "ngx-toastr";

@Component({
	selector: "app-answer-request",
	templateUrl: "./answer-request.component.html",
	styleUrls: ["./answer-request.component.scss"]
})
export class AnswerRequestComponent implements OnInit {
	events: RequestedEvent[] = [];
	selectedSlot = new Map<string | undefined, Date>();

	constructor(
		private toastr: ToastrService,
		private dataService: DataService,
		public dialogRef: MatDialogRef<AnswerRequestComponent>,
		public matDialog: MatDialog,
		@Optional()
		@Inject(MAT_DIALOG_DATA)
		public data: { events: RequestedEvent[] }
	) {
		this.events = data.events;
	}

	ngOnInit(): void {}

	cancel(): void {
		this.dialogRef.close();
	}

	async accept(): Promise<void> {
		const eventIds = [...this.selectedSlot.entries()].map(([id]) => id);
		const events: GuruEvent[] = [];
		this.events.forEach((e) => {
			if (eventIds.includes(e._id)) {
				events.push({
					message: e.message,
					courseId: e.course._id,
					guruId: e.guru._id,
					requesterId: e.requesterId,
					time: this.selectedSlot.get(e._id),
					link: e.link
				});
			}
		});
		await this.dataService.saveEvents(events);
		await this.dataService.deleteEventRequest(eventIds);
		this.toastr.success("Your reply has been sent", "", {
			positionClass: "toast-bottom-center"
		});
		this.cancel();
	}

	selectSlot(slot: Date, id: string | undefined): void {
		if (!this.selectedSlot.has(id)) {
			this.selectedSlot.set(id, slot);
		} else {
			this.selectedSlot.delete(id);
		}
	}

	isSlotSelected(slot: Date, id: string | undefined): boolean {
		if (this.selectedSlot.get(id) === slot) {
			return true;
		}
		return false;
	}
}
