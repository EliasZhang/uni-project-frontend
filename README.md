<!-- PROJECT LOGO -->
<div align="center">
 <a href="https://gitlab.com/EliasZhang/uni-project-frontend">
    <img src="logo.png" alt="Logo" width="200" height="200">
  </a>
  <h2>Learn like a Guru</h2>
  <p>An awesome app to highlight your life!</p>
  <a href="https://test-guru-app-v1.herokuapp.com/">View Demo</a>
    ·
  <a href="https://gitlab.com/EliasZhang/uni-project-frontend/-/issues">Report Bug</a>
    ·
  <a href="https://gitlab.com/EliasZhang/uni-project-frontend/-/issues">Request Feature</a>
<br/>
<br/>
</div>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#features">Features</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#running-project">Run project</a></li>
      </ul>
    </li>
    <li><a href="#build">Build</a></li>
    <li><a href="#deploy">Deploy</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#Contributor">Contributor</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

It is a university project. The goal of this project is helping the university students to find the students who study the same course, or the guru who has completed this course and who can help student study the courses . :smile:

Here's why:
* During corona pandemic, it is hard to find colleagues face to face.    
* There are some similar platforms in the internet but all of them are not well-done.

### Features
* Registration / Login 
![login](https://gitlab.com/EliasZhang/uni-project-frontend/-/raw/master/screenshots/loginAndReg.PNG)
* Chat functionality using socket.io
	* course group chat / private messages
	* mark messages as read
	* show when communication partner is typing
![chat](  
https://gitlab.com/EliasZhang/uni-project-frontend/-/raw/master/screenshots/chat.PNG)
*   Profile pictures (stored on the server due to performance limitations)
![pic](https://gitlab.com/EliasZhang/uni-project-frontend/-/raw/master/screenshots/pic.PNG)
*   Request Guru event / Accept request
*  Live notifications about incoming requests and acceptance of the sent requests; live update of the information on the dashboard (using socket.io)
![dashboard](https://gitlab.com/EliasZhang/uni-project-frontend/-/raw/master/screenshots/dashboard.PNG)
*   Rating Guru events
 ![rate](https://gitlab.com/EliasZhang/uni-project-frontend/-/raw/master/screenshots/rate.PNG)

### Built With

This section lists any major frameworks that we built our project using.
* [mongoDB](https://expressjs.com/)
* [Express](https://getbootstrap.com)
* [Angular](https://angular.io/)
* Node.js

(MEAN Stack)

<!-- GETTING STARTED -->
## Getting Started

This is an instructions of how you set up your project locally.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* node.js and npm
  download node.js and npm at [here](https://nodejs.org/en/download/)
  
* git
    download git at [here](https://git-scm.com/downloads)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/EliasZhang/uni-project-frontend.git
   ```
2. Install frontend NPM packages
   ```sh
   cd client
   npm install
   ```
3. Install backend NPM packages
   ```sh
   cd server
   npm install
   ```

### Run project

1. Run backend
   ```sh
   cd server
   npm run serve
   ```
   open http://localhost:3000
   
2. Run frontend
   ```sh
   cd client
   ng serve
   ``` 
   open http://localhost:4200/

<!-- Build EXAMPLES -->
## Build

Only frontend needs to be built. After running the following code, the built frontend will be in the dist folder in the server folder. Open http://localhost:3000/app/login. Cool!!! The frontend can be opened in the backend
   ```sh
   cd client
   npm run build
   ```


<!-- Deploy -->
## Deploy

Thanks to CI/CD, any commits pushed in the **deploy** branch will be automatically deployed. **Build is not needed for deploy**

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contributor
<table>
  <tr>
    <td align="center"><img src="https://static.wikia.nocookie.net/marveldatabase/images/f/fe/Avengers_Endgame_poster_041_Variant_Textless.jpg/revision/latest/scale-to-width-down/333?cb=20190629185509" width="100px;" alt=""/><br /><sub><b>Arcadio Herrera</b></sub><br /></td>
    <td align="center"><img src="https://assets.thalia.media/img/artikel/065f39b0ed2479696ca15310da4f2c166cec0fc7-00-03.jpeg" width="100px;" alt=""/><br /><sub><b>Martin Jose Garcia Muñoz</b></sub><br /></td>
    <td align="center"><img src="https://lumiere-a.akamaihd.net/v1/images/image_b97b56f3.jpeg?region=0%2C0%2C540%2C810" width="100px;" alt=""/><br /><sub><b>Nathalie</b></sub><br /></td>
    <td align="center"><img src="https://musicimage.xboxlive.com/catalog/video.movie.8D6KGWXN35GS/image?locale=de-de&mode=crop&purposes=BoxArt&q=90&h=300&w=200&format=jpg" width="100px;" alt=""/><br /><sub><b>Olga Filipova</b></sub><br /></td>
    <td align="center"><img src="https://www.reuters.com/resizer/JA7vvGMAmdbu5vMrpDvnjpzF8vo=/960x0/filters:quality(80)/cloudfront-us-east-2.images.arcpublishing.com/reuters/PSBXQI7AQVO5PJK6TB5ATDCXWY.jpg" width="100px;" alt=""/><br /><sub><b>Yili Zhang</b></sub><br /></td>
  </tr>
</table>


