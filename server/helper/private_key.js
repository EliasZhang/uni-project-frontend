const crypto = require("crypto");

// eslint-disable-next-line func-names
const generateKey = function () {
    return crypto.randomBytes(32).toString("hex");
};

module.exports = generateKey;
