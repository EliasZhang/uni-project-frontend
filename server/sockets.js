const repository = require("./message_repository");

const msgSocket = (socket, io) => {
    socket.on("subscribe", (room) => {
        console.log("joining room", room);
        socket.join(room);
        socket.on("private-message", (msg) => {
            repository.saveMessage(msg).then((id)=> {
                repository.getUserById(msg.senderId).then((user) => {
                    const message = {
                        _id: id,
                        text: msg.message,
                        user: {
                            _id: user._id,
                            name: user.name,
                            profileImageId: user.profileImageId,
                        },
                        time: msg.time,
                        readBy: [],
                    };
                    io.in(room).emit("private-post", message);
                });
            });
        });
        socket.on("user_is_typing", (user) => {
            io.in(room).emit("user_is_typing", user);
        });
    }); 
}

const newGuruRequestNotification = (socket, io) => {
    socket.on("send_guru_request", (user_id) => {
        io.emit("guru_request_created_" + user_id);
    });
}

const deleteGuruRequestNotification = (socket, io) => {
    socket.on("delete_outgoing_request", () => {
        io.emit("delete_outgoing_request");
    });
}

const newPrivateMessageNotification = (socket, io) => {
    socket.on("send_private_msg", (obj) => {
        io.emit("private_msg_notification_" + obj.to, {from: obj.from, courseId: obj.courseId});
    });
}

const newGroupMessageNotification = (socket, io) => {
    socket.on("send_group_msg", () => {
        io.emit("group_msg_notification");
    });
}

const newUpcomingEventsNotification = (socket, io) => {
    socket.on("events_update", () => {
        io.emit("events_update");
    });
}

const msgReadNotification = (socket, io) => {
    socket.on("msg_read_live", (data) => {
        io.emit("msg_read_live", {readBy: data.readBy, msgId: data.msgId});
    });
}

const joinCourseNotification = (socket, io) => {
    socket.on("somebody_joined_course", () => {
        io.emit("somebody_joined_course");
    });
}

const leaveCourseNotification = (socket, io) => {
    socket.on("leave_course", () => {
        io.emit("leave_course");
    });
}

module.exports = {
    msgSocket,
    newGuruRequestNotification,
    newPrivateMessageNotification,
    newGroupMessageNotification,
    newUpcomingEventsNotification,
    deleteGuruRequestNotification,
    msgReadNotification,
    joinCourseNotification,
    leaveCourseNotification
};