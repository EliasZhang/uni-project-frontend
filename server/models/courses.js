const mongoose = require("mongoose");

const { Schema } = mongoose;

const courseSchema = new Schema({
    name: { type: String },
    guru_ids: { type: Array, ref: "Guru" },
    member_ids: { type: Array },
});

module.exports = mongoose.model("Course", courseSchema);
