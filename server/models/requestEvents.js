const mongoose = require("mongoose");

const { Schema } = mongoose;

const eventRequestSchema = new Schema({
    message: { type: String },
    courseId: { type: mongoose.Types.ObjectId, ref: "Course" },
    requesterId: { type: mongoose.Types.ObjectId },
    guruId: { type: mongoose.Types.ObjectId, ref: "Guru" },
    suggestedTimes: { type: Array },
    link: { type: String },
});

module.exports = mongoose.model("EventRequest", eventRequestSchema);
