const mongoose = require("mongoose");

const { Schema } = mongoose;

const Course = require("./courses");

const guruSchema = new Schema({
    user_id: { type: mongoose.Types.ObjectId, ref: "User", required: true },
    level: { type: String, required: true },
    karma: { type: Number, required: true },
});

guruSchema.pre("deleteMany", async (next) => {
    Course.updateMany({ guru_ids: this._id }, { $pull: { guru_ids: this._id } });
    return next();
});

module.exports = mongoose.model("Guru", guruSchema);
