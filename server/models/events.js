const mongoose = require("mongoose");

const { Schema } = mongoose;

const eventSchema = new Schema({
    message: { type: String },
    courseId: { type: mongoose.Types.ObjectId, ref: "Course" },
    guruId: { type: mongoose.Types.ObjectId, ref: "Guru" },
    requesterId: { type: mongoose.Types.ObjectId },
    time: { type: Date },
    link: { type: String },
});

module.exports = mongoose.model("Event", eventSchema);
