const mongoose = require("mongoose");

const { Schema } = mongoose;

const messageSchema = new Schema({
    senderId: { type: mongoose.Types.ObjectId, ref: "User", required: true },
    receiverId: { type: mongoose.Types.ObjectId, required: false },
    courseId: { type: mongoose.Types.ObjectId, required: true },
    message: { type: String, required: true },
    time: { type: Date, required: true },
    readBy: { type: Array, ref: "User", required: false },
});

module.exports = mongoose.model("Message", messageSchema);
