const mongoose = require("mongoose");

const { Schema } = mongoose;
const bcrypt = require("bcrypt");

const validator = require("../validator/validator");

const emailValidation = [
    { validator: validator.emailCheckValid, msg: "email is not valid" },
];

const userSchema = new Schema({
    email: { type: String, required: true, validate: emailValidation },
    name: { type: String, required: true },
    password: { type: String, required: true },
    creation_dt: { type: Date, default: Date.now },
    profileImageId: { type: String, required: false },
});

// eslint-disable-next-line func-names
userSchema.statics.hashPassword = function (password) {
    return password === undefined ? undefined : bcrypt.hashSync(password, 10);
};

// eslint-disable-next-line func-names
userSchema.statics.checkDuplicateEmail = async function (email) {
    const duplicatedEmail = await this.findOne({ email });
    return duplicatedEmail !== null;
};

userSchema.methods.isValid = function (hashedpassword) {
    return bcrypt.compareSync(hashedpassword, this.password);
};

module.exports = mongoose.model("User", userSchema);
