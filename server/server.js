const express = require("express");
const { msgSocket, 
    newGuruRequestNotification,
    newPrivateMessageNotification,
    newGroupMessageNotification,
    newUpcomingEventsNotification,
    deleteGuruRequestNotification,
    msgReadNotification,
    joinCourseNotification,
    leaveCourseNotification} = require("./sockets");

const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
    cors: {
        origin: "*",
    },
});

const bodyParser = require("body-parser");

app.use(bodyParser.json());

const cors = require("cors");

app.use(cors({
    origin: "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
}));

app.use(express.static('images'));

app.use("/", express.static("dist"));
app.get("/app*", (req, res) => {
    res.sendFile("index.html", { root: "dist" });
});

const userRouter = require("./routers/users");
const coursesRouter = require("./routers/courses");
const messagesRouter = require("./routers/messages");
const eventRequestRouter = require("./routers/requestEvents");
const eventRouter = require("./routers/events");
const guruRouter = require("./routers/gurus");

app.use("/users", userRouter);
app.use("/courses", coursesRouter);
app.use("/messages", messagesRouter);
app.use("/requestEvents", eventRequestRouter);
app.use("/events", eventRouter);
app.use("/gurus", guruRouter);

io.on("connection", (socket) => {
    msgSocket(socket, io); 
    newGuruRequestNotification(socket, io);
    newPrivateMessageNotification(socket, io);
    newGroupMessageNotification(socket, io);
    newUpcomingEventsNotification(socket, io);
    deleteGuruRequestNotification(socket, io);
    msgReadNotification(socket, io);
    joinCourseNotification(socket, io);
    leaveCourseNotification(socket, io);
});

const connectDB = require("./mongoDB/connection");

connectDB.then(() => {
    console.log("Db is connected");
    const port = process.env.PORT || 3000;
    http.listen(+port, () => {
        console.log(`Server is listening on http://localhost:${port}`);
    });
}).catch((error) => {
    console.log(error);
});
