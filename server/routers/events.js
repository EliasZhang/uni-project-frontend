const express = require("express");

const router = express.Router();

const Event = require("../models/events");

const Guru = require("../models/gurus");
const Token = require("../session/token");
const Course = require("../models/courses");

var mongoose = require('mongoose');

module.exports = router;

router.post("/saveEvents", async (req, res) => {
    Event.insertMany(req.body);
    res.send();
});

router.get("/getEventsForCurrentUser", async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const courseIds = (await Course.find({ member_ids: mongoose.Types.ObjectId(user._id) },
            { _id: 1 })).map((c) => c._id);
        const events = await Event.find({ courseId: { $in: courseIds } })
            .populate("courseId")
            .populate("guruId")
            .populate({
                path: "guruId",
                populate: {
                    path: "user_id",
                    model: "User",
                },
            });
        return res.status(200).send(events.map((e) => ({
            _id: e._id,
            message: e.message,
            courseId: e.courseId._id,
            courseName: e.courseId.name,
            guruId: e.guruId._id,
            requesterId: e.requesterId,
            guruName: e.guruId.user_id.name,
            time: e.time,
            link: e.link,
        })));
    } catch (e) {
        console.error(e);
        return res.status(500).send("Could not get events for current users.");
    }
});

router.get("/getEventsByCourseId", async (req, res) => {
    try {
        const query = await Event.find({ courseId: req.query.id })
            .populate("courseId")
            .populate("guruId")
            .populate({
                path: "guruId",
                populate: {
                    path: "user_id",
                    model: "User",
                },
            });
        const result = [];
        query.forEach((e) => {
            result.push({
                _id: e._id,
                message: e.message,
                courseId: e.courseId.name,
                guruId: e.guruId._id,
                requesterId: e.requesterId,
                guruName: e.guruId.user_id.name,
                time: e.time,
                link: e.link,
            });
        });
        res.send(result);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get events by course id.");
    }
});

router.post("/deleteEvent", async (req, res) => {
    try {
        Event.deleteOne(
            {
                _id: req.body.id,
            },
            (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not delete event.");
    }
});

router.get("/getAcceptedRequestsForGuru", async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const gurus = await Guru.find({ user_id: user._id }).lean();
        const guru_ids = gurus.map(({ _id }) => _id)
        if (gurus) {
            let query = await Event.find({ guruId: { $in: guru_ids }})
                .populate("courseId")
                .populate("guruId")
                .populate({
                    path: "guruId",
                    populate: {
                        path: "user_id",
                        model: "User",
                    },
                });

            query = query.map((e) => ({
                _id: e._id,
                message: e.message,
                course: {
                    _id: e.courseId._id,
                    name: e.courseId.name,
                    guru_ids: e.courseId.guru_ids,
                    member_ids: e.courseId.member_ids,
                },
                requesterId: e.requesterId,
                guru: {
                    _id: e.guruId._id,
                    user: {
                        _id: e.guruId.user_id._id,
                        name: e.guruId.user_id.name,
                        profileImageId: e.guruId.user_id.profileImageId,
                    },
                    level: e.guruId.level,
                    karma: e.guruId.karma,
                },
                time: e.time,
                link: e.link,
            }));
            res.send(query);
        } else {
            res.send([]);
        }
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get accepted requests.");
    }
});

router.post("/deleteEvents", async (req, res) => {
    try {
        Event.deleteMany(
            {
                _id: { $in: req.body.id },
            },
            (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not delete events.");
    }
});
