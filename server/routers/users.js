const express = require("express");

const multer = require("multer");
const { v4 } = require("uuid");
const fs = require("fs");

const storage = multer.diskStorage({
    destination: "images",
    filename: (req, file, cb) => cb(null, `${v4()}.png`),
});

const upload = multer({ storage });

const router = express.Router();
const User = require("../models/users");
const Course = require("../models/courses");
const Token = require("../session/token");

router.post("/register", async (req, res) => {
    try {
        const user = new User({
            email: req.body.email,
            name: req.body.name,
            password: User.hashPassword(req.body.password),
            creation_dt: Date.now(),
        });
        const duplicatedEmail = await User.checkDuplicateEmail(req.body.email);

        if (duplicatedEmail) {
            return res.status(400).send("This email address is already in use.");
        }

        const loginUser = await user.save();
        return res.status(200).json(Token.tokenGenerator({ _id: loginUser._id }));
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not register.");
    }
});

router.post("/delete-profile-image", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const path = `images/${user.profileImageId}`;

        fs.unlink(path, (err) => {
            if (err) {
                console.error(err);
            }
        });

        await User.findByIdAndUpdate(user._id,
            { $set: { profileImageId: undefined } });
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not delete profile picture.");
    }
});

router.post("/edit-profile", upload.single("profileImg"), Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const userData = {};
        if (req.file && req.file.filename) {
            userData.profileImageId = req.file.filename;
        }
        if (req.body.email) {
            const duplicatedEmail = await User.checkDuplicateEmail(req.body.email);
            if (duplicatedEmail && req.body.email !== user.email) {
                return res.status(400).send("email is used");
            }
            userData.email = req.body.email;
        }
        if (req.body.name) {
            userData.name = req.body.name;
        }
        if (req.body.password) {
            userData.password = User.hashPassword(req.body.password);
        }
        try {
            await User.findByIdAndUpdate(user._id,
                { $set: userData },
                { new: true });
            return res.status(200).send();
        } catch (err) {
            return res.status(401).send(err);
        }
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not update profile.");
    }
});

router.post("/login", async (req, res) => {
    try {
        const loginUser = await User.findOne({ email: req.body.email });
        if (loginUser) {
            if (loginUser.isValid(req.body.password)) {
                const token = Token.tokenGenerator({ _id: loginUser._id });
                return res.status(200).json(token);
            }
            return res.status(401).json({ message: "Invalid Credentials" });
        } return res.status(401).send("user doesn't exist");
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not login.");
    }
});

router.get("/loggedInUser", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        res.status(200).json(user);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get logged-in user.");
    }
});

router.get("/getUserById", Token.verifyToken, async (req, res) => {
    try {
        let query = await User.findOne({ _id: req.query.id });
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get user.");
    }
});

router.get("/profileData", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        res.status(200).json({
            _id: user._id,
            name: user.name,
            email: user.email,
            profileImageId: user.profileImageId,
        });
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get profile data.");
    }
});

router.get("/byCourseId", Token.verifyToken, async (req, res) => {
    try {
        const course = await Course.findOne({ _id: req.query.courseId });
        let query = await User.find({ _id: course.member_ids });
        query = query.map((x) => {
            if (x.profileImageId) {
                return { _id: x._id, name: x.name, profileImageId: x.profileImageId };
            }
            return { _id: x._id, name: x.name, profileImageId: "" };
        });
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get course members.");
    }
});

module.exports = router;
