const express = require("express");

const router = express.Router();

const Token = require("../session/token");

const Messages = require("../models/messages");

module.exports = router;

router.get("/groupChat", Token.verifyToken, async (req, res) => {
    try {
        const msg = await Messages.find(
            { courseId: req.query.id, receiverId: undefined },
            { message: 1, time: 1, readBy: 1 },
        )
            .populate("senderId")
            .populate("readBy");
        const m = msg.map((data) => {
            if (data.senderId.profileImageId) {
                return {
                    _id: data._id,
                    text: data.message,
                    user: {
                        _id: data.senderId._id,
                        name: data.senderId.name,
                        profileImageId: data.senderId.profileImageId,
                    },
                    time: data.time,
                    readBy: data.readBy,
                };
            }
            return {
                _id: data._id,
                text: data.message,
                user: {
                    _id: data.senderId._id,
                    name: data.senderId.name,
                    profileImageId: "",
                },
                time: data.time,
                readBy: data.readBy,
            };
        });
        res.send(m);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get messages.");
    }
});

router.get("/privateChat", Token.verifyToken, async (req, res) => {
    try {
        const msg = await Messages.find({
            $or:
            [
                { courseId: req.query.id, receiverId: req.query.r_id, senderId: req.query.s_id },
                { courseId: req.query.id, receiverId: req.query.s_id, senderId: req.query.r_id },
            ],
        },
        { message: 1, time: 1, readBy: 1 })
            .populate("senderId")
            .populate("readBy");
        const m = msg.map((data) => ({
            _id: data._id,
            text: data.message,
            user: {
                _id: data.senderId._id,
                name: data.senderId.name,
                profileImageId: data.senderId.profileImageId,
            },
            time: data.time,
            readBy: data.readBy,
        }));
        res.send(m);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get private messages.");
    }
});

router.post("/markMsgsAsRead", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        Messages.updateMany(
            { _id: { $in: req.body.ids } },
            { $addToSet: { readBy: [user._id] } }, (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not mark messages as read.");
    }
});

router.get("/getNewGroupMessagesCount", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const msg = await Messages.find(
            {
                courseId: req.query.id,
                receiverId: undefined,
                senderId: { $ne: user._id },
                readBy: { $nin: [user._id] },
            },
            { senderId: 1, _id: 0 },
        );

        const count = await Messages.find(
            {
                courseId: req.query.id,
                receiverId: undefined,
                senderId: { $ne: user._id },
                readBy: { $nin: [user._id] },
            },
        ).countDocuments();

        const result = {
            count,
            senderIds: msg,
        };
        res.send(result);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get messages count.");
    }
});

router.get("/getNewPrivateMessages", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const msg = await Messages.find(
            {
                courseId: req.query.id,
                receiverId: user._id,
                readBy: { $nin: [user._id] },
            },
            { senderId: 1, _id: 0 },
        );

        const count = await Messages.find(
            {
                courseId: req.query.id,
                receiverId: user._id,
                readBy: { $nin: [user._id] },
            },
        ).countDocuments();

        const result = {
            count,
            senderIds: msg,
        };
        res.send(result);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get new private messages count.");
    }
});
