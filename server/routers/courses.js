const express = require("express");

const router = express.Router();

const Course = require("../models/courses");

const Token = require("../session/token");

module.exports = router;

router.post("/enrollUser", Token.verifyToken, async (req, res) => {
    try {
        const { courseId } = req.body;
        const user = await Token.getCurrentUser(req);
        Course.findOne({ _id: courseId }, (err, course) => {
            course.member_ids.push(user._id);
            course.save();
            res.send();
        });
    } catch (e) {
        console.error(e);
        res.status(500).send("User could not join course.");
    }
});

router.post("/create", Token.verifyToken, async (req, res) => {
    try {
        const { name } = req.body;
        const user = await Token.getCurrentUser(req);
        Course.create(
            {
                name,
            },
            (err, item) => {
                item.member_ids.push(user._id);
                item.save();
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Course could not be created.");
    }
});

router.get("/allCourseNames", async (req, res) => {
    try {
        const courseNames = await Course.find({}, { name: 1 });
        return res.send(courseNames.map((c) => c.name));
    } catch (e) {
        console.error(e);
        return res.status(500).send("Could not get course names.");
    }
});

router.get("/allNotEnrolled", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        if (user) {
            const query = await Course.find({ member_ids: { $ne: user._id } });
            return res.send(query);
        }
        return res.status(401).send("User not logged in.");
    } catch (e) {
        console.error(e);
        return res.status(500).send("Could not get completable courses.");
    }
});

router.get("/allNotEnrolledNotGuru", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        if (user) {
            const notMemberCourses = await Course.find({ member_ids: { $ne: user._id } }).populate("guru_ids");
            const notGuruCourses = notMemberCourses.filter((c) => {
                const gurus = c.guru_ids;
                if (gurus) {
                    return !gurus.some((guru) => guru.user_id.toString() === user._id.toString());
                }
                return true;
            });
            return res.send(notGuruCourses);
        }
        return res.status(401).send("User not logged in.");
    } catch (e) {
        console.error(e);
        return res.status(500).send("Could not get joinable courses.");
    }
});

router.get("/forCurrentUser", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        if (user) {
            const query = await Course.find({ member_ids: user._id });
            return res.send(query);
        }
        return res.status(401).send("User not logged in.");
    } catch (e) {
        console.error(e);
        return res.status(500).send("Could not get courses.");
    }
});

router.get("/courseById", Token.verifyToken, async (req, res) => {
    try {
        const query = await Course.findOne({ _id: req.query.courseId });
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get course.");
    }
});

router.post("/leaveCourse", Token.verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        Course.findOneAndUpdate(
            { _id: req.body.courseId },
            { $pull: { member_ids: { $in: [user._id] } } }, (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not leave course.");
    }
});
