const express = require("express");

const router = express.Router();

const RequestEvent = require("../models/requestEvents");

module.exports = router;

router.post("/requestEvent", async (req, res) => {
    try {
        RequestEvent.create(
            {
                message: req.body.message,
                courseId: req.body.courseId,
                requesterId: req.body.requesterId,
                guruId: req.body.guruId,
                suggestedTimes: req.body.suggestedTimes,
                link: req.body.link,
            },
            (err, item) => {
                item.save();
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not request event.");
    }
});

router.post("/deleteRequestEvent", async (req, res) => {
    try {
        RequestEvent.deleteOne(
            {
                _id: req.body.requestId,
            },
            (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not delete event request.");
    }
});

router.get("/getRequestsForUser", async (req, res) => {
    try {
        let query = await RequestEvent.find().populate("courseId")
            .populate("guruId")
            .populate({
                path: "guruId",
                populate: {
                    path: "user_id",
                    model: "User",
                },
            });
        query = query.filter((e) => e.guruId.user_id._id.toString() === req.query.id)
            .map((e) => ({
                _id: e._id,
                message: e.message,
                course: {
                    _id: e.courseId._id,
                    name: e.courseId.name,
                    guru_ids: e.courseId.guru_ids,
                    member_ids: e.courseId.member_ids,
                },
                requesterId: e.requesterId,
                guru: {
                    _id: e.guruId._id,
                    user: {
                        _id: e.guruId.user_id._id,
                        name: e.guruId.user_id.name,
                        profileImageId: e.guruId.user_id.profileImageId,
                    },
                    level: e.guruId.level,
                    karma: e.guruId.karma,
                },
                suggestedTimes: e.suggestedTimes,
                link: e.link,
            }));
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get requests for user.");
    }
});

router.get("/getOutgoingRequests", async (req, res) => {
    try {
        let query = await RequestEvent.find({ requesterId: req.query.id })
            .populate("courseId")
            .populate("guruId")
            .populate({
                path: "guruId",
                populate: {
                    path: "user_id",
                    model: "User",
                },
            });

        query = query.map((e) => ({
            _id: e._id,
            message: e.message,
            course: {
                _id: e.courseId._id,
                name: e.courseId.name,
                guru_ids: e.courseId.guru_ids,
                member_ids: e.courseId.member_ids,
            },
            requesterId: e.requesterId,
            guru: {
                _id: e.guruId._id,
                user: {
                    _id: e.guruId.user_id._id,
                    name: e.guruId.user_id.name,
                    profileImageId: e.guruId.user_id.profileImageId,
                },
                level: e.guruId.level,
                karma: e.guruId.karma,
            },
            suggestedTimes: e.suggestedTimes,
            link: e.link,
        }));
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get outgoing requests.");
    }
});

router.post("/deleteRequests", async (req, res) => {
    try {
        RequestEvent.deleteMany(
            {
                _id: { $in: req.body.id },
            },
            (err, item) => {
                res.send(item);
            },
        );
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not delete requests.");
    }
});
