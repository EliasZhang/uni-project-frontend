const express = require("express");

const router = express.Router();

const Guru = require("../models/gurus");
const User = require("../models/users");

const Course = require("../models/courses");

const Token = require("../session/token");
const { verifyToken } = require("../session/token");

module.exports = router;

router.post("/updateGuruships", async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        const submittedGuruIds = req.body.filter((g) => g._id).map((g) => g._id);

        await Guru.deleteMany({
            user_id: user._id,
            _id: { $nin: submittedGuruIds },
        });

        req.body.forEach(async (guruship) => {
            if (!guruship._id) {
                Guru.create(
                    {
                        user_id: user._id,
                        level: guruship.proficiency,
                        karma: 0,
                    },
                    (err, item) => {
                        submittedGuruIds.push(item._id);
                        Course.findOne({ _id: guruship.course._id }, (e, course) => {
                            course.guru_ids.push(item._id);
                            course.save();
                        });
                    },
                );
            }
        });
        res.send();
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not update guruships.");
    }
});

router.get("/gurushipsForCurrentUser", verifyToken, async (req, res) => {
    try {
        const user = await Token.getCurrentUser(req);
        try {
            const gurus = (await Guru.find(
                { user_id: user._id },
                { _id: 1 },
            )).map((g) => g._id);
            const courses = await Course.find({ guru_ids: { $in: gurus } }).populate("guru_ids");
            const guruships = [];
            courses.forEach(
                (c) => c.guru_ids.forEach(
                    (g) => {
                        if (g.user_id.toString() === user._id.toString()) {
                            guruships.push({
                                _id: g._id,
                                course: c,
                                proficiency: g.level,
                            });
                        }
                    },
                ),
            );
            res.send(guruships);
        } catch (e) {
            res.send([]);
        }
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get guruships.");
    }
});

router.get("/gurusByIds", Token.verifyToken, async (req, res) => {
    try {
        const ids = req.query.ids.split(", ");
        const query = await Guru.find({ userId: { $in: ids } });
        res.send(query);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get Gurus.");
    }
});

router.get("/gurusByCourseId", Token.verifyToken, async (req, res) => {
    try {
        const guruIds = await Course.findOne(
            { _id: req.query.id },
            { _id: 0, guru_ids: 1 },
        );
        const idsAsArray = guruIds.guru_ids;
        const gurus = await Guru.find({ _id: { $in: idsAsArray } }).populate("user_id");
        const result = gurus.map((g) => ({
            _id: g._id,
            user: {
                _id: g.user_id._id,
                name: g.user_id.name,
                profileImageId: g.user_id.profileImageId,
            },
            level: g.level,
            karma: g.karma,
        }));
        res.send(result);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get Gurus.");
    }
});

router.get("/getGuruByGuruId", Token.verifyToken, async (req, res) => {
    try {
        const guru = await Guru.findOne({ _id: req.query.id });
        const userId = guru.user_id;
        const user = await User.findOne({ _id: userId });
        const result = {
            _id: guru._id,
            user: {
                _id: user._id,
                name: user.name,
                profileImageId: user.profileImageId,
            },
            level: guru.level,
            karma: guru.karma,
        };
        res.send(result);
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not get Guru.");
    }
});

router.post("/updateGurusKarma", Token.verifyToken, async (req, res) => {
    try {
        Guru.findOne({_id: req.body.id}, (err, guru) => {
            const newGuru = guru;
            newGuru.karma += 1;
            newGuru.save();
            res.send();
        });
    } catch (e) {
        console.error(e);
        res.status(500).send("Could not update Karma.");
    }
});
