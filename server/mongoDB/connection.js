const mongoose = require("mongoose");

const dbUri = "mongodb+srv://adminadmin:adminadmin@gurucluster.v5cqc.mongodb.net/guruApp?retryWrites=true&w=majority";

const connectDB = mongoose.connect(dbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
});

module.exports = connectDB;
