const jwt = require("jsonwebtoken");
const User = require("../models/users");

const PrivateKey = require("../helper/private_key");

const verifyToken = function verifyToken(req, res, next) {
    const { token } = req.query;
    const { privateKey } = req.query;

    jwt.verify(token, privateKey, (err) => {
        if (err) {
            return res.status(401).json({ message: "Unauthorized request" });
        }
        return next();
    });
};

const tokenGenerator = function tokenGenerator(payload) {
    const privateKey = PrivateKey();
    const token = jwt.sign(payload, privateKey, { expiresIn: "3h" });
    return { token, privateKey };
};

const decodeToken = function decodeToken(token) {
    return jwt.decode(token);
};

const getCurrentUser = async function getCurrentUser(req) {
    const decoded = decodeToken(req.query.token);
    const { _id } = decoded;
    const user = await User.findOne({ _id });
    return user;
};

module.exports = {
    verifyToken,
    tokenGenerator,
    decodeToken,
    getCurrentUser,
};
