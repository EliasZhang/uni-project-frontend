const Message = require("./models/messages");
const User = require("./models/users");

module.exports = {
    async getUserById(userId) {
        const query = await User.findOne({ _id: userId });
        return query;
    },
    saveMessage: (message) => new Promise((resolve, reject) => {
        Message.create(
            message,
            (err, item) => {
                if (err) {
                    console.log(err);
                    reject(err)
                }
                msg = item;
                item.save();
                resolve(item._id)
            },
        );
    }),
};
